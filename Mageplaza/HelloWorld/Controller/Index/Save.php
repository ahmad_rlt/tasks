<?php

namespace Mageplaza\HelloWorld\Controller\Index;

use Mageplaza\HelloWorld\Model\PostFactory;
use Mageplaza\HelloWorld\Model\PostRepository;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class Save implements ActionInterface
{
    protected Http $request;
    protected ResultFactory $resultRedirectFactory;
    /**
     * @var ModelFactory
     */
    private ModelFactory $modelFactory;
    /**
     * @var ModelRepository
     */
    private ModelRepository $modelRepository;

    /**
     * Index constructor.
     * @param Context $context
     * @param ResultFactory $resultRedirectFactory
     * @param PostFactory $modelFactory
     * @param PostRepository $modelRepository
     * @param Http $request
     */
    public function __construct(
        Context $context,
        ResultFactory $resultRedirectFactory,
        Http $request,
        PostFactory $modelFactory,
        PostRepository $modelRepository
    ) {
        $this->request = $request;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->PostFactory = $modelFactory;
        $this->PostRepository = $modelRepository;
    }

    /**
     * @return ResultInterface
     * @throws CouldNotSaveException
     */
    public function execute()
    {
        $obj = $this->modelFactory->create();
        $data = $this->request->getParams();
        $this->modelRepository->save($obj->addData($data)); // Service Contract
        //$obj->addData($data)->save(); // Model / Resource Model
        $resultRedirect = $this->resultRedirectFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('mageplaza_helloworld/index/EmployeeRegistrationForm');
        return $resultRedirect;
    }
}
