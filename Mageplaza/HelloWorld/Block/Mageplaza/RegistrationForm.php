<?php

namespace Mageplaza\HelloWorld\Block\Mageplaza;

use Magento\Framework\View\Element\Template;

class RegistrationForm extends Template
{
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSubmitFormUrl(): string
    {
        return $this->getUrl('helloworld/index/submitform');
    }
}
