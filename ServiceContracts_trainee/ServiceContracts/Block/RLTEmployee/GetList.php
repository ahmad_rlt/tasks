<?php

namespace RLTSquare\ServiceContracts\Block\RLTEmployee;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use RLTSquare\ServiceContracts\Api\RLTEmployeeRepositoryInterface;

class GetList extends \Magento\Framework\View\Element\Template
{
    /** @var SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var FilterBuilder */
    private $filterBuilder;

    /** @var RLTEmployeeRepositoryInterface */
    private $rltEmployeeRepository;

    public function __construct(
        Template\Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        RLTEmployeeRepositoryInterface $rltEmployeeRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->rltEmployeeRepository = $rltEmployeeRepository;
    }

    public function getEmployeeList() {
        $filters[] = $this->filterBuilder
            ->setField('employee_email')
            ->setConditionType('like')
            ->setValue('%athar%')
            ->create();

        $this->searchCriteriaBuilder->addFilters($filters);

        /** @var \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->rltEmployeeRepository->getList($searchCriteria);
        return $result->getItems();
    }
}
