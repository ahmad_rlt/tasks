<?php

namespace Mageplaza\HelloWorld\Controller\Adminhtml\Post;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Mageplaza\HelloWorld\Model\PostFactory;
use Mageplaza\HelloWorld\Model\ResourceModel\Post\CollectionFactory;
use Mageplaza\HelloWorld\Model\ResourceModel\Post as PostResource;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected Filter $filter;
    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var PostFactory
     */
    protected PostFactory $extensionModelFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    protected PostResource $postResource;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Filter $filter
     * @param ScopeConfigInterface $scopeConfig
     * @param PostFactory $extensionModelFactory
     * @param CollectionFactory $collectionFactory
     * @param PostResource $postResource
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Filter $filter,
        ScopeConfigInterface $scopeConfig,
        PostFactory $extensionModelFactory,
        CollectionFactory $collectionFactory,
        PostResource $postResource
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->filter = $filter;
        $this->scopeConfig = $scopeConfig;
        $this->extensionModelFactory = $extensionModelFactory;
        $this->collectionFactory = $collectionFactory;
        $this->postResource=$postResource;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws AlreadyExistsException
     */
    public function execute()
    {
        $post = $this->extensionModelFactory->create();
        $data = $this->getRequest()->getParams();

        $post->setData($data);
        $this->postResource->save($post);
        return $this->resultRedirectFactory->create()->setPath('mageplaza_helloworld/post/index');
    }


    protected function _isAllowed()
    {
        return true;
    }
}
