<?php

namespace Mageplaza\HelloWorld\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Mageplaza\HelloWolrd\Api\Data\MageplazaHelloWorldInterface;
use Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldSearchResultInterface;

/**
 * Interface RLTEmployeeRepositoryInterface
 * @package RLTSquare\ServiceContracts\Api
 */
interface MageplazaHelloWorldRepositoryInterface
{
    /**
     * @param $id
     * @return MageplazaHelloWorldInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param $name
     * @return MageplazaHelloWorldInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByName($name);

    /**
     * @param MageplazaHelloWorldInterface $RLTEmployee
     * @return MageplazaHelloWorldInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(MageplazaHelloWorldInterface $RLTEmployee);

    /**
     * @param MageplazaHelloWorldInterface $RLTEmployee
     * @return boolean
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(MageplazaHelloWorldInterface $RLTEmployee);

    /**
     * @param $id
     * @return boolean
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return MageplazaHelloWorldSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
