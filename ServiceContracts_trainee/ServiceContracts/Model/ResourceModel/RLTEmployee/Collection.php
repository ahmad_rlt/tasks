<?php

namespace RLTSquare\ServiceContracts\Model\ResourceModel\RLTEmployee;

/**
 * Class Collection
 * @package RLTSquare\ServiceContracts\Model\ResourceModel\RLTEmployee
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';
    protected $_eventPrefix = 'rlt_employees';
    protected $_eventObject = 'employee_collection';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            'RLTSquare\ServiceContracts\Model\RLTEmployee',
            'RLTSquare\ServiceContracts\Model\ResourceModel\RLTEmployee'
        );
    }
}
