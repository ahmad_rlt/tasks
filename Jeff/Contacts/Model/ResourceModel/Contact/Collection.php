<?php
namespace Jeff\Contacts\Model\ResourceModel\Contact;

/**
 *start of class
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'jeff_contacts_contact_id';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jeff\Contacts\Model\Contact','Jeff\Contacts\Model\ResourceModel\Contact');
    }
}
