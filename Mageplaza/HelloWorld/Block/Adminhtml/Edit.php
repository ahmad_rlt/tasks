<?php
namespace Mageplaza\HelloWorld\Block\Adminhtml;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Mageplaza_HelloWorld';
        $this->_controller = 'adminhtml';
        parent::_construct();
        $this->buttonList->remove('delete');
    }
}
