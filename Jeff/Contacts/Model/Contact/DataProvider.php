<?php

namespace Jeff\Contacts\Model\Contact;

use Jeff\Contacts\Model\ResourceModel\Contact\Collection;
use Jeff\Contacts\Model\ResourceModel\Contact\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 *start of class
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;
    /**
     * @var
     */
    protected $loadedData;

    /**
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $contactCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $contactCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $contactCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $contact) {
            $this->loadedData[$contact->getId()] = $contact->getData();
        }
        return $this->loadedData;
    }
}
