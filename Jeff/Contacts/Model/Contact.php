<?php

namespace Jeff\Contacts\Model;

use Magento\Framework\Model\AbstractModel;

/**
 *start of class
 */
class Contact extends AbstractModel
{
    /**
     *declaration of constant
     */
    const CACHE_TAG = 'jeff_contacts_contact';

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Jeff\Contacts\Model\ResourceModel\Contact');
    }
}
