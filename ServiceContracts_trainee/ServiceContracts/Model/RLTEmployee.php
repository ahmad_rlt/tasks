<?php

namespace RLTSquare\ServiceContracts\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeInterface;

/**
 * Class RLTEmployee
 * @package RLTSquare\ServiceContracts\Model
 */
class RLTEmployee extends \Magento\Framework\Model\AbstractModel implements RLTEmployeeInterface
{
    const CACHE_TAG = 'rlt_employees';

    protected $_cacheTag = 'rlt_employees';

    protected $_eventPrefix = 'rlt_employees';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            'RLTSquare\ServiceContracts\Model\ResourceModel\RLTEmployee'
        );
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    /**
     * @inheritDoc
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeName($employeeName)
    {
        return $this->setData(self::NAME, $employeeName);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeDesignation()
    {
        return $this->getData(self::DESIGNATION);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeDesignation($employeeDesignation)
    {
        return $this->setData(self::DESIGNATION, $employeeDesignation);
    }

    /**
     * @inheritDoc
     */
    public function getEmployeeEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setEmployeeEmail($employeeEmail)
    {
        return $this->setData(self::EMAIL, $employeeEmail);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

//    /**
//     * @inheritDoc
//     */
//    public function getExtensionAttributes()
//    {
//        return $this->_getExtensionAttributes();
//    }
//
//    /**
//     * @inheritDoc
//     */
//    public function setExtensionAttributes(
//        \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//    )
//    {
//        $this->_setExtensionAttributes($extensionAttributes);
//    }
}
