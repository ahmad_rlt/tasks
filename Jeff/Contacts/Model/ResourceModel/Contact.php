<?php
namespace Jeff\Contacts\Model\ResourceModel;

/**
 *start of class
 */
class Contact extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jeff_contacts_contact','jeff_contacts_contact_id');
    }
}
