<?php

namespace RLTSquare\ServiceContracts\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeInterface;
use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeSearchResultInterface;

/**
 * Interface RLTEmployeeRepositoryInterface
 * @package RLTSquare\ServiceContracts\Api
 */
interface RLTEmployeeRepositoryInterface
{
    /**
     * @param $id
     * @return RLTEmployeeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

        /**
     * @param $email
     * @return RLTEmployeeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByEmail($email);

    /**
     * @param RLTEmployeeInterface $RLTEmployee
     * @return RLTEmployeeInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(RLTEmployeeInterface $RLTEmployee);

    /**
     * @param RLTEmployeeInterface $RLTEmployee
     * @return boolean
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(RLTEmployeeInterface $RLTEmployee);

    /**
     * @param $id
     * @return boolean
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return RLTEmployeeSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
