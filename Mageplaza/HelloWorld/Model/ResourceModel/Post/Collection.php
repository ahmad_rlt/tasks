<?php
namespace Mageplaza\HelloWorld\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'mageplaza_helloworld_post_collection';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function construct()
    {
        $this->init(\Mageplaza\HelloWorld\Model\Post::class, \Mageplaza\HelloWorld\Model\ResourceModel\Post::class);
    }

}
