<?php
namespace Mageplaza\HelloWorld\Controller\Adminhtml;

class Index extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Customer::manage');
    }
}
