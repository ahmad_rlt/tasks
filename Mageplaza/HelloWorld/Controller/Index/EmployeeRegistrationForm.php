<?php

namespace Mageplaza\HelloWorld\Controller\Index;

use Magento\Framework\App\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class EmployeeRegistrationForm
 * @package Mageplaza\HelloWorld\Controller\Index
 */
class EmployeeRegistrationForm extends \Magento\Framework\App\Action\Action
{
    /** @var PageFactory */
    private $pageFactory;

    /**
     * EmployeeRegistrationForm constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $pageFactory = $this->pageFactory->create();
        $pageFactory->getConfig()->getTitle()->set('WellCome to Form');
        return $pageFactory;
    }
}
