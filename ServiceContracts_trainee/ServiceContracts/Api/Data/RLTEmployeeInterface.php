<?php

namespace RLTSquare\ServiceContracts\Api\Data;

//use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface RLTEmployeeInterface
 * @package RLTSquare\ServiceContracts\Api\Data
 */
interface RLTEmployeeInterface
{
    /*
     * Entity ID.
     */
    const ENTITY_ID = 'entity_id';

    /*
     * Created-at timestamp.
     */
    const CREATED_AT = 'created_at';

    /*
     * Updated-at timestamp.
     */
    const UPDATED_AT = 'updated_at';

    const NAME = 'employee_name';
    const EMAIL = 'employee_email';
    const DESIGNATION = 'employee_designation';

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return string|null
     */
    public function getEmployeeName();

    /**
     * @param $employeeName
     * @return void
     */
    public function setEmployeeName($employeeName);

    /**
     * @return string|null
     */
    public function getEmployeeEmail();

    /**
     * @param $employeeEmail
     * @return $this
     */
    public function setEmployeeEmail($employeeEmail);

    /**
     * @return string|null
     */
    public function getEmployeeDesignation();

    /**
     * @param $employeeDesignation
     * @return $this
     */
    public function setEmployeeDesignation($employeeDesignation);

    /**
     * @return string|null Created-at timestamp.
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt timestamp
     * @return $this
     */
    public function setCreatedAt($createdAt);

//    /**
//     * @return \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface|null
//     */
//    public function getExtensionAttributes();
//
//    /**
//     * @param \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//     * @return void
//     */
//    public function setExtensionAttributes(
//        \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//    );
}
