<?php

namespace Mageplaza\HelloWorld\Api\Data;

use Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldInterface;

/**
 * Interface MageplazaHelloWorldInterface
 * @package Mageplaza\HelloWorld\Api\Data
 */
interface MageplazaHelloWorldSearchResultInterface
{
    /**
     * @return \Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return mixed
     */
    public function setItems(array $items);
}
