<?php

namespace Mageplaza\HelloWorld\Controller\Index;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\UrlInterface;
use Mageplaza\HelloWorld\Api\MageplazaHelloWorldRepositoryInterface;
use Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldInterface;
use Mageplaza\HelloWorld\Model\Post;
use Mageplaza\HelloWorld\Model\PostFactory;

class SubmitForm extends Action
{
    /** @var Validator */
    private $formKeyValidator;

    /** @var UrlInterface */
    private $urlModel;

    /** @var PostFactory */
    private $PostFactory;


    /**
     * SubmitForm constructor.
     * @param Context $context
     * @param UrlInterface $urlModel
     * @param Validator $formKeyValidator
     * @param PostFactory $PostFactory
     * @param MageplazaHelloWorldRepositoryInterface $EmployeeRepository
     */
    public function __construct(
        Context $context,
        UrlInterface $urlModel,
        Validator $formKeyValidator,
        PostFactory $PostFactory,
        MageplazaHelloWorldRepositoryInterface $EmployeeRepository
    ) {
        parent::__construct($context);
        $this->urlModel = $urlModel;
        $this->formKeyValidator = $formKeyValidator;
        $this->PostFactory = $PostFactory;
        $this->PostRepository = $EmployeeRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->getRequest()->isPost()
            || !$this->formKeyValidator->validate($this->getRequest())
        ) {
            $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
            $this->messageManager->addErrorMessage(
                __('Invalid Form Key, Please Refresh the Page')
            );
            return $this->resultRedirectFactory->create()
                ->setUrl($this->_redirect->error($url));
        }

        $employeeEmail = $this->getRequest()->getParam(MageplazaHelloWorldInterface::ANAME);
        try {
            $this->isEmailExists($employeeEmail);
            $requestData = $this->getRequest()->getParams();

            /** @var RLTEmployee $employObject */
            $employObject = $this->PostFactory->create();
            $employObject->setName($requestData['name']);
            $employObject->setaname($requestData['aname']);
            $employObject->setdescription($requestData['description']);

            $this->EmployeeRepository->save($employObject);
            $this->messageManager->addSuccessMessage(
                __('Employee with email %1 saved successfully.', $employeeEmail)
            );
            $url = $this->urlModel->getUrl('*/*/
            employeeregistrationform', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->success($url));
            return $resultRedirect;

            /**
             * Do not use this way of setting the data for an entity.
             */
            $employObject->setData($this->getRequest()->getParams());
            if ($employObject->save()) {
                $this->messageManager->addSuccessMessage(
                    __('Employee with email %1 saved successfully.', $employeeEmail)
                );
                $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
                return $resultRedirect;
            }
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while saving the data of employee: ' . $exception->getMessage())
            );
        }

        $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
        $resultRedirect->setUrl($this->_redirect->error($url));
        return $resultRedirect;
    }

    /**
     * @param $email
     * @return bool
     * @throws AlreadyExistsException
     */
    private function isEmailExists($email)
    {
        try {
            $employee = $this->PostRepository->getByEmail($email);
            if ($employee->getEntityId() !== null) {
                throw new AlreadyExistsException(__('Employee with this email already exists.'));
            } else {
                return false;
            }
        } catch (AlreadyExistsException $alreadyExistsException) {
            throw new AlreadyExistsException(__($alreadyExistsException->getMessage()));
        }
    }
}
