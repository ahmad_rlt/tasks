<?php

namespace Mageplaza\HelloWorld\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Mageplaza\HelloWorld\Model\ResourceModel\Post\CollectionFactory;
use Mageplaza\HelloWorld\Model\ResourceModel\Post\Collection;
use Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldSearchResultInterfaceFactory;
use Mageplaza\HelloWorld\Model\PostFactory;
use Mageplaza\HelloWorld\Model\ResourceModel\Post as ResourceModelRLTEmployee;
use Mageplaza\HelloWorld\Api\Data\MageplazaHelloWorldInterface;

/**
 * Class RLTEmployeeRepository
 * @package Mageplaza\HelloWorld\Model
 */
class PostRepository implements MageplazaHelloWorldRepositoryInterface
{
    /** @var CollectionFactory */
    private $collectionFactory;

    /** @var MageplazaHelloWorldSearchResultInterfaceFactory */
    private MageplazaHelloWorldSearchResultInterfaceFactory $ResultInterfaceFactory;

    /** @var \Mageplaza\HelloWorld\Model\PostFactory */
    private $employeeFactory;

    /** @var ResourceModelPost */
    private ResourceModelPost $resourceModelPost;
    private MageplazaHelloWorldSearchResultInterfaceFactory $MageplazaHelloWorldSearchResultInterfaceFactory;
    private \Mageplaza\HelloWorld\Model\PostFactory $PostFactory;

    /**
     * RLTEmployeeRepository constructor.
     * @param CollectionFactory $collectionFactory
     * @param MageplazaHelloWorldSearchResultInterfaceFactory $employeeSearchResultInterfaceFactory
     * @param \Mageplaza\HelloWorld\Model\PostFactory $employeeFactory
     * @param ResourceModelPost $resourceModelPost
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        MageplazaHelloWorldSearchResultInterfaceFactory $employeeSearchResultInterfaceFactory,
        PostFactory $employeeFactory,
        ResourceModelPost $resourceModelPost
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->MageplazaHelloWorldSearchResultInterfaceFactory = $employeeSearchResultInterfaceFactory;
        $this->PostFactory = $employeeFactory;
        $this->resourceModelPost = $resourceModelPost;
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        $Post = $this->employeeFactory->create();
        $this->resourceModelPost->load($Post, $id);
        if (!$Post) {
            throw new NoSuchEntityException(
                __('Object with id "%1" does not exist.', $id)
            );
        }
        return $Post;
    }

    /**
     * @inheritDoc
     */
    public function getByEmail($email)
    {
        $Post = $this->employeeFactory->create();
        $this->resourceModelPost->load($Post, $email, 'aname');
        if (!$Post) {
            throw new NoSuchEntityException(
                __('Object with email "%1" already exists.', $email)
            );
        }
        return $Post;
    }

    /**
     * @inheritDoc
     */
    public function delete(MageplazaHelloWorldInterface $object)
    {
        try {
            $this->resourceModelPost->delete($object);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id)
    {
        try {
            $this->delete($this->getById($id));
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
    }

    /**
     * @inheritDoc
     */
    public function save(MageplazaHelloWorldInterface $RLTEmployee)
    {
        try {
            $this->resourceModelPost->save($RLTEmployee);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->employeeSearchResultInterfaceFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

}
