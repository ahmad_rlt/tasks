<?php

namespace RLTSquare\ServiceContracts\Model;

use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeSearchResultInterface;
use Magento\Framework\Api\SearchResults;

class RLTEmployeeSearchResult extends SearchResults implements RLTEmployeeSearchResultInterface
{
}
