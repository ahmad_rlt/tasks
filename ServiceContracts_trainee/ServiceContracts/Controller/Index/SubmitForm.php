<?php

namespace RLTSquare\ServiceContracts\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\UrlInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use RLTSquare\ServiceContracts\Model\RLTEmployeeFactory;
use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeInterface;
use RLTSquare\ServiceContracts\Api\RLTEmployeeRepositoryInterface;

/**
 * Class SubmitForm
 * @package RLTSquare\ServiceContracts\Controller\Index
 */
class SubmitForm extends Action
{
    /** @var Validator */
    private $formKeyValidator;

    /** @var UrlInterface */
    private $urlModel;

    /** @var RLTEmployeeFactory */
    private $rltEmployeeFactory;

    /** @var RLTEmployeeRepositoryInterface */
    private $rltEmployeeRepository;

    /**
     * SubmitForm constructor.
     * @param Context $context
     * @param UrlInterface $urlModel
     * @param Validator $formKeyValidator
     * @param RLTEmployeeFactory $rltEmployeeFactory
     * @param RLTEmployeeRepositoryInterface $rltEmployeeRepository
     */
    public function __construct(
        Context $context,
        UrlInterface $urlModel,
        Validator $formKeyValidator,
        RLTEmployeeFactory $rltEmployeeFactory,
        RLTEmployeeRepositoryInterface $rltEmployeeRepository
    ) {
        parent::__construct($context);
        $this->urlModel = $urlModel;
        $this->formKeyValidator = $formKeyValidator;
        $this->rltEmployeeFactory = $rltEmployeeFactory;
        $this->rltEmployeeRepository = $rltEmployeeRepository;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->getRequest()->isPost()
            || !$this->formKeyValidator->validate($this->getRequest())
        ) {
            $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
            $this->messageManager->addErrorMessage(
                __('Invalid Form Key, Please Refresh the Page')
            );
            return $this->resultRedirectFactory->create()
                ->setUrl($this->_redirect->error($url));
        }

        $employeeEmail = $this->getRequest()->getParam(RLTEmployeeInterface::EMAIL);
        try {
            $this->isEmailExists($employeeEmail);
            $requestData = $this->getRequest()->getParams();

            /** @var \RLTSquare\ServiceContracts\Model\RLTEmployee $employObject */
            $employObject = $this->rltEmployeeFactory->create();
            $employObject->setEmployeeName($requestData['employee_name']);
            $employObject->setEmployeeEmail($requestData['employee_email']);
            $employObject->setEmployeeDesignation($requestData['employee_designation']);

            $this->rltEmployeeRepository->save($employObject);
            $this->messageManager->addSuccessMessage(
                __('Employee with email %1 saved successfully.', $employeeEmail)
            );
            $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->success($url));
            return $resultRedirect;

            /**
             * Do not use this way of setting the data for an entity.
             */
//            $employObject->setData($this->getRequest()->getParams());
//            if ($employObject->save()) {
//                $this->messageManager->addSuccessMessage(
//                    __('Employee with email %1 saved successfully.', $employeeEmail)
//                );
//                $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
//                $resultRedirect->setUrl($this->_redirect->success($url));
//                return $resultRedirect;
//            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while saving the data of employee: ' . $exception->getMessage())
            );
        }

        $url = $this->urlModel->getUrl('*/*/employeeregistrationform', ['_secure' => true]);
        $resultRedirect->setUrl($this->_redirect->error($url));
        return $resultRedirect;
    }

    /**
     * @param $email
     * @return bool
     * @throws AlreadyExistsException
     */
    private function isEmailExists($email)
    {
        try {
            $employee = $this->rltEmployeeRepository->getByEmail($email);
            if ($employee->getEntityId() !== null) {
                throw new AlreadyExistsException(__('Employee with this email already exists.'));
            } else {
                return false;
            }
        } catch (AlreadyExistsException $alreadyExistsException) {
            throw new AlreadyExistsException(__($alreadyExistsException->getMessage()));
        }
    }
}
