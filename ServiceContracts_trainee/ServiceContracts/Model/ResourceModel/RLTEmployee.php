<?php

namespace RLTSquare\ServiceContracts\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class RLTEmployee
 * @package RLTSquare\ServiceContracts\Model\ResourceModel
 */
class RLTEmployee extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * RLTEmployee constructor.
     * @param Context $context
     * @param null $connectionName
     */
    public function __construct(Context $context, $connectionName = null)
    {
        parent::__construct($context, $connectionName);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('rlt_employees', 'entity_id');
    }
}
