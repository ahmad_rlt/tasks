<?php

namespace Mageplaza\HelloWorld\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Mageplaza\HelloWorld\Model\PostFactory;
use Magento\Store\Model\ScopeInterface;

class Image extends Template
{

    /**
     * @var PostFactory
     */
    protected PostFactory $testFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param PostFactory $testFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Context $context,
        PostFactory $testFactory
    ) {
        $this->testFactory = $testFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function getTaskCollection()
    {
        $test = $this->testFactory->create();
        return $test->getCollection();
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/width', $storeScope);
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/height', $storeScope);
    }

    /**
     * @return mixed
     */
    public function getFormValue()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/enable', $storeScope);
    }
}
