<?php

namespace Mageplaza\HelloWorld\Block\Mageplaza;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Mageplaza\HelloWorld\Api\MageplazaHelloWorldRepositoryInterface;

class GetList extends \Magento\Framework\View\Element\Template
{
    /** @var SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var FilterBuilder */
    private $filterBuilder;

    /** @var MageplazaHelloWorldRepositoryInterface */
    private $EmployeeRepository;

    public function __construct(
        Template\Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        MageplazaHelloWorldRepositoryInterface $EmployeeRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->MageplazaHelloWorldRepositoryInterface = $EmployeeRepository;
    }

    public function getEmployeeList() {
        $filters[] = $this->filterBuilder
            ->setField('employee_email')
            ->setConditionType('like')
            ->setValue('%athar%')
            ->create();

        $this->searchCriteriaBuilder->addFilters($filters);

        /** @var \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->EmployeeRepository->getList($searchCriteria);
        return $result->getItems();
    }
}
