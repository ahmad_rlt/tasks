<?php
namespace Mageplaza\HelloWorld\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Mageplaza\HelloWorld\Model\PostFactory;
use Magento\Framework\View\Element\Template;

class Display extends \Magento\Framework\View\Element\Template
{
    /**
     * @var PostFactory
     */
    protected PostFactory $testFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param PostFactory $testFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Context $context,
        \Mageplaza\HelloWorld\Model\PostFactory $postFactory,

        array $data = [],
        PostFactory $testFactory
    ) {
        $this->testFactory = $testFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
        $this->_postFactory = $postFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getTaskCollection()
    {
        $test = $this->testFactory->create();
        return $test->getCollection();
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/width', $storeScope);
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/height', $storeScope);
    }

    /**
     * @return mixed
     */
    public function getFormValue()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue('mageplaza_sec2/general/enable', $storeScope);
    }


    public function sayHello()
    {
        return __('NAME=Muhammad Ahmad Mehmood ');
    }

    public function sayHello1()
    {
        return __('City=Lahore ');
    }

    public function sayHello4()
    {
        return __('Education="Graduation');
    }

    public function sayHello2()
    {
        return __('12-04-1997');
    }

    public function sayHello3()
    {
        return __('LinkedIn');
    }

    public function sayHello5()
    {
        return __('<img src="pub/media/folder/2.png" alt="">');
    }
    public function getConfigValue($value = '')
    {
        return $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    public function getPostCollection(){
        $post = $this->_postFactory->create();
        return $post->getCollection();
    }

}
