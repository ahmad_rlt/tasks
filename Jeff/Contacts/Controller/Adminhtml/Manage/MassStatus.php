<?php
namespace Jeff\Contacts\Controller\Adminhtml\Manage;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;
use Jeff\Contacts\Model\ContactFactory;
use Jeff\Contacts\Model\ResourceModel\Contact\CollectionFactory;
use Jeff\Contacts\Model\ResourceModel\Contact as PostResource;

class MassStatus extends Action
{
    /**
     * @var Filter
     */
    protected Filter $filter;
    /**
     * @var PageFactory
     */
    protected PageFactory $resultPageFactory;
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var ContactFactory
     */
    protected ContactFactory $extensionModelFactory;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    protected PostResource $postResource;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Filter $filter
     * @param ScopeConfigInterface $scopeConfig
     * @param ContactFactory $extensionModelFactory
     * @param CollectionFactory $collectionFactory
     * @param PostResource $postResource
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Filter $filter,
        ScopeConfigInterface $scopeConfig,
        ContactFactory $extensionModelFactory,
        CollectionFactory $collectionFactory,
        PostResource $postResource
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->filter = $filter;
        $this->scopeConfig = $scopeConfig;
        $this->extensionModelFactory = $extensionModelFactory;
        $this->collectionFactory = $collectionFactory;
        $this->postResource=$postResource;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $updated = 0;
            foreach ($collection as $item) {
                $model = $this->extensionModelFactory->create()->load($item['jeff_contacts_contact_id']);
                $model->setData('is_active', $this->getRequest()->getData('is_active'));
                $this->postResource->save($model);
                $updated++;
            }
            if ($updated) {
                $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were updated.', $updated));
            }

        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Failed', $e->getMessage()));
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return true;
    }
}
