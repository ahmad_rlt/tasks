<?php

namespace RLTSquare\ServiceContracts\Api\Data;

use RLTSquare\ServiceContracts\Api\Data\RLTEmployeeInterface;

/**
 * Interface RLTEmployeeSearchResultInterface
 * @package RLTSquare\ServiceContracts\Api\Data
 */
interface RLTEmployeeSearchResultInterface
{
    /**
     * @return \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return mixed
     */
    public function setItems(array $items);
}
