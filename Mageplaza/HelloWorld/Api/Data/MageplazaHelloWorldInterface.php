<?php

namespace Mageplaza\HelloWorld\Api\Data;

//use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface MageplazaHelloWorldInterface
 * @package Mageplaza\HelloWorld\Api\Data
 */
interface MageplazaHelloWorldInterface
{
    /*
     * ID.
     */
    const ID = 'id';

    /*
     * Created-at timestamp.
     */
    const hour = 'hour';

    /*
     * Updated-at timestamp.
     */
    const UPDATED_AT = 'updated_at';

    const NAME = 'name';
    const ANAME = 'aname';
    const description = 'description';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $Id
     * @return $this
     */
    public function setId($Id);

    /**
     * @return string|null
     */
    public function getName();

    /**
     * @param $name
     * @return void
     */
    public function setName($name);

    /**
     * @return string|null
     */
    public function getaname();

    /**
     * @param $aname
     * @return $this
     */
    public function setaname($aname);

    /**
     * @return string|null
     */
    public function getdescription();

    /**
     * @param $description
     * @return $this
     */
    public function setdescription($description);

    /**
     * @return string|null Created-at timestamp.
     */
    public function gethour();

    /**
     * @param string $hour timestamp
     * @return $this
     */
    public function sethour($hour);

//    /**
//     * @return \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface|null
//     */
//    public function getExtensionAttributes();
//
//    /**
//     * @param \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//     * @return void
//     */
//    public function setExtensionAttributes(
//        \RLTSquare\ServiceContracts\Api\Data\RLTEmployeeExtensionInterface $extensionAttributes
//    );
}
