<?php

namespace RLTSquare\ServiceContracts\Block\RLTEmployee;

use Magento\Framework\View\Element\Template;

/**
 * Class RegistrationForm
 * @package RLTSquare\ServiceContracts\Block\RLTEmployee
 */
class RegistrationForm extends Template
{
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSubmitFormUrl(): string
    {
        return $this->getUrl('service/index/submitform');
    }
}
